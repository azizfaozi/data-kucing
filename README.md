# Data Science for Cats

## Data Gathering

I make web service that it will collect data about data condition with cats. So, you cant insert data using form on

https://faoziaziz.herokuapp.com

then if you want to get the dataset just using api to access it, you will get csv.

## WebAPI
faoziaziz-hku directory contain web-api if you wanna change the api you must give me credit on CREDIT 

## Build The Documentation

```bash
sphinx-build -b html notes/source  faoziaziz-hku/src/main/resources/static/doc

```